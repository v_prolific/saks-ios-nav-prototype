//
//  ViewController.swift
//  Saks Nav Proto
//
//  Created by VIRAKRI JINANGKUL on 3/24/16.
//  Copyright © 2016 VIRAKRI JINANGKUL. All rights reserved.
//

import UIKit

struct categoryListStruct {
    let id: Int
    let name: String
    let parent: Int
    let haveChild: Bool
}

class ViewController: UIViewController {

//MARK: - Struct Define
    
    @IBOutlet var navBar: UINavigationBar!

    @IBOutlet var imagePlaceHolderView: UIImageView!

    @IBOutlet var loadingOverlayView: UIView!
    
    @IBOutlet var searchButton: UIImageView!
    
    @IBOutlet var menuButtonAnimationView: menuIconAnimationView!
    @IBOutlet var viewNameLabel: UILabel!
    
    @IBOutlet var viewNameLabelContraint: NSLayoutConstraint!
    
    @IBOutlet var searchButtonContraint: NSLayoutConstraint!
    
    @IBOutlet var saksLogoBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet var saksLogoImage: UIImageView!
    
    @IBOutlet var menuDisplayView: UIView!
    var mainMenuViewController:mainMenuVC?
    var mainSearchViewController:mainSearchVC?
    var toggleVar : Bool = false
    
    @IBOutlet var menuButton: UIButton!
    var categoryArray = [categoryListStruct(id: 1, name: "Designers", parent: 0, haveChild: false),
        categoryListStruct(id: 2, name: "women's apparel", parent: 0, haveChild: true),
        categoryListStruct(id: 3, name: "Shoes", parent: 0, haveChild: true),
        categoryListStruct(id: 4, name: "Handbags", parent: 0, haveChild: true),
        categoryListStruct(id: 5, name: "Jewelry & Accessories", parent: 0, haveChild: true),
        categoryListStruct(id: 6, name: "Beauty", parent: 0, haveChild: false),
        categoryListStruct(id: 7, name: "Men", parent: 0, haveChild: true),
        categoryListStruct(id: 8, name: "Kids", parent: 0, haveChild: false),
        categoryListStruct(id: 9, name: "Home", parent: 0, haveChild: false),
        categoryListStruct(id: 10, name: "Gifts", parent: 0, haveChild: false),
        categoryListStruct(id: 11, name: "Sale", parent: 0, haveChild: false),
        categoryListStruct(id: 12, name: "Best Sellers", parent: 2, haveChild: false),
        categoryListStruct(id: 13, name: "New Arrivals", parent: 2, haveChild: false),
        categoryListStruct(id: 14, name: "Dresses", parent: 2, haveChild: true),
        categoryListStruct(id: 15, name: "Formal & Evening", parent: 2, haveChild: false),
        categoryListStruct(id: 16, name: "Tops", parent: 2, haveChild: true),
        categoryListStruct(id: 17, name: "Jackets & Vests", parent: 2, haveChild: true),
        categoryListStruct(id: 18, name: "Jeans", parent: 2, haveChild: true),
        categoryListStruct(id: 19, name: "Pants & Shorts", parent: 2, haveChild: true),
        categoryListStruct(id: 20, name: "Jumpsuits", parent: 2, haveChild: false),
        categoryListStruct(id: 21, name: "Skirts", parent: 2, haveChild: false),
        categoryListStruct(id: 22, name: "Coats", parent: 2, haveChild: false),
        categoryListStruct(id: 23, name: "All Dresses", parent: 14, haveChild: false),
        categoryListStruct(id: 24, name: "Evening", parent: 14, haveChild: false),
        categoryListStruct(id: 25, name: "Cocktail", parent: 14, haveChild: false),
        categoryListStruct(id: 26, name: "Day", parent: 14, haveChild: false),
        categoryListStruct(id: 27, name: "Workwear", parent: 14, haveChild: false),
        categoryListStruct(id: 28, name: "Party Dresses", parent: 14, haveChild: false),
        categoryListStruct(id: 29, name: "Maxi", parent: 14, haveChild: false),
        categoryListStruct(id: 30, name: "Mini", parent: 14, haveChild: false),
        categoryListStruct(id: 31, name: "Mother of the Bride", parent: 14, haveChild: false),
        categoryListStruct(id: 32, name: "Bridesmaid", parent: 14, haveChild: false),
        categoryListStruct(id: 33, name: "Prom", parent: 14, haveChild: false),
        categoryListStruct(id: 34, name: "All tops", parent: 16, haveChild: false),
        categoryListStruct(id: 35, name: "blouses", parent: 16, haveChild: false),
        categoryListStruct(id: 36, name: "t-shirts&tanks", parent: 16, haveChild: false),
        categoryListStruct(id: 37, name: "party tops", parent: 16, haveChild: false),
        categoryListStruct(id: 38, name: "collared & buton downs", parent: 16, haveChild: false),
        categoryListStruct(id: 39, name: "tunics", parent: 16, haveChild: false),
        categoryListStruct(id: 40, name: "sweatshirts", parent: 16, haveChild: false),
        categoryListStruct(id: 41, name: "long sleve", parent: 16, haveChild: false),
        categoryListStruct(id: 42, name: "short sleeve", parent: 16, haveChild: false),
        categoryListStruct(id: 43, name: "sleeveless", parent: 16, haveChild: false),
        categoryListStruct(id: 44, name: "All Jackets & vests", parent: 17, haveChild: false),
        categoryListStruct(id: 45, name: "leather & faux leather", parent: 17, haveChild: false),
        categoryListStruct(id: 46, name: "Jackets & Vests", parent: 17, haveChild: false),
        categoryListStruct(id: 47, name: "blazers", parent: 17, haveChild: false),
        categoryListStruct(id: 48, name: "vests", parent: 17, haveChild: false),
        categoryListStruct(id: 49, name: "All jeans", parent: 18, haveChild: false),
        categoryListStruct(id: 50, name: "skinny", parent: 18, haveChild: false),
        categoryListStruct(id: 51, name: "boyfriend", parent: 18, haveChild: false),
        categoryListStruct(id: 52, name: "flared", parent: 18, haveChild: false),
        categoryListStruct(id: 53, name: "bootcut", parent: 18, haveChild: false),
        categoryListStruct(id: 54, name: "straight", parent: 18, haveChild: false),
        categoryListStruct(id: 55, name: "distressed", parent: 18, haveChild: false),
        categoryListStruct(id: 56, name: "cropped", parent: 18, haveChild: false),
        categoryListStruct(id: 57, name: "shorts", parent: 18, haveChild: false),
        categoryListStruct(id: 58, name: "all pants & shorts", parent: 19, haveChild: false),
        categoryListStruct(id: 59, name: "Pants & Shorts", parent: 19, haveChild: false),
        categoryListStruct(id: 60, name: "shorts", parent: 19, haveChild: false),
        categoryListStruct(id: 61, name: "cropped", parent: 19, haveChild: false),
        categoryListStruct(id: 62, name: "leather & faux leather", parent: 19, haveChild: false),
        categoryListStruct(id: 63, name: "leggings", parent: 19, haveChild: false),
        categoryListStruct(id: 64, name: "shoes", parent: 3, haveChild: false),
        categoryListStruct(id: 65, name: "new & popular", parent: 3, haveChild: false),
        categoryListStruct(id: 66, name: "mens shoes", parent: 3, haveChild: false),
        categoryListStruct(id: 67, name: "kids shoes", parent: 3, haveChild: false),
        categoryListStruct(id: 68, name: "handbags", parent: 4, haveChild: false),
        categoryListStruct(id: 69, name: "wallets & cases", parent: 4, haveChild: false),
        categoryListStruct(id: 70, name: "new & popular", parent: 4, haveChild: false),
        categoryListStruct(id: 71, name: "all jewelry", parent: 5, haveChild: true),
        categoryListStruct(id: 72, name: "fashion jewelry", parent: 5, haveChild: true),
        categoryListStruct(id: 73, name: "fine jewelry", parent: 5, haveChild: true),
        categoryListStruct(id: 74, name: "watches", parent: 5, haveChild: true),
        categoryListStruct(id: 75, name: "sunglasses & opticals", parent: 5, haveChild: true),
        categoryListStruct(id: 76, name: "accessories", parent: 5, haveChild: false),
        categoryListStruct(id: 77, name: "new & popular", parent: 5, haveChild: false),
        categoryListStruct(id: 78, name: "All All Jewelry", parent: 71, haveChild: false),
        categoryListStruct(id: 79, name: "new arrivals", parent: 71, haveChild: false),
        categoryListStruct(id: 80, name: "earrings", parent: 71, haveChild: false),
        categoryListStruct(id: 81, name: "necklaces", parent: 71, haveChild: false),
        categoryListStruct(id: 82, name: "bracelets", parent: 71, haveChild: false),
        categoryListStruct(id: 83, name: "rings", parent: 71, haveChild: false),
        categoryListStruct(id: 84, name: "brooces", parent: 71, haveChild: false),
        categoryListStruct(id: 85, name: "jewelry storage", parent: 71, haveChild: false),
        categoryListStruct(id: 86, name: "All Fashion Jewelry", parent: 72, haveChild: false),
        categoryListStruct(id: 87, name: "earrings", parent: 72, haveChild: false),
        categoryListStruct(id: 88, name: "necklaces", parent: 72, haveChild: false),
        categoryListStruct(id: 89, name: "bracelets", parent: 72, haveChild: false),
        categoryListStruct(id: 90, name: "rings", parent: 72, haveChild: false),
        categoryListStruct(id: 91, name: "earrings", parent: 73, haveChild: false),
        categoryListStruct(id: 92, name: "necklaces", parent: 73, haveChild: false),
        categoryListStruct(id: 93, name: "bracelets", parent: 73, haveChild: false),
        categoryListStruct(id: 94, name: "rings", parent: 73, haveChild: false),
        categoryListStruct(id: 95, name: "All watches", parent: 74, haveChild: false),
        categoryListStruct(id: 96, name: "for her", parent: 74, haveChild: false),
        categoryListStruct(id: 97, name: "for him", parent: 74, haveChild: false),
        categoryListStruct(id: 98, name: "all sunglasses & opticals", parent: 75, haveChild: false),
        categoryListStruct(id: 99, name: "aviators", parent: 75, haveChild: false),
        categoryListStruct(id: 100, name: "oversized", parent: 75, haveChild: false),
        categoryListStruct(id: 101, name: "cat's-eye", parent: 75, haveChild: false),
        categoryListStruct(id: 102, name: "square & rectangle", parent: 75, haveChild: false),
        categoryListStruct(id: 103, name: "round & oval", parent: 75, haveChild: false),
        categoryListStruct(id: 104, name: "mirrored", parent: 75, haveChild: false),
        categoryListStruct(id: 105, name: "opticals", parent: 75, haveChild: false),
        
        categoryListStruct(id: 107, name: "shoes", parent: 7, haveChild: true),
        categoryListStruct(id: 106, name: "Apparel", parent: 7, haveChild: true),
        categoryListStruct(id: 108, name: "accessories", parent: 7, haveChild: false),
        categoryListStruct(id: 109, name: "cuff links, jewelry & watches", parent: 7, haveChild: true),
        categoryListStruct(id: 110, name: "grooming & cologne", parent: 7, haveChild: false),
        categoryListStruct(id: 111, name: "sports & workout", parent: 7, haveChild: false),
        categoryListStruct(id: 112, name: "new & popular", parent: 7, haveChild: false),
        categoryListStruct(id: 113, name: "sports & leisure", parent: 7, haveChild: false),
        categoryListStruct(id: 114, name: "All apparel", parent: 106, haveChild: false),
        categoryListStruct(id: 115, name: "coats & jackets", parent: 106, haveChild: true),
        categoryListStruct(id: 116, name: "suits, sportcoats & vests", parent: 106, haveChild: true),
        categoryListStruct(id: 117, name: "t-shirts & polos", parent: 106, haveChild: true),
        categoryListStruct(id: 118, name: "casual button-down shirts", parent: 106, haveChild: false),
        categoryListStruct(id: 119, name: "dress shirts", parent: 106, haveChild: true),
        categoryListStruct(id: 120, name: "sweaters & sweatshirts", parent: 106, haveChild: true),
        categoryListStruct(id: 121, name: "jeans", parent: 106, haveChild: true),
        categoryListStruct(id: 122, name: "pants & shorts", parent: 106, haveChild: false),
        categoryListStruct(id: 123, name: "swimwear", parent: 106, haveChild: false),
        categoryListStruct(id: 124, name: "activewear", parent: 106, haveChild: false),
        categoryListStruct(id: 125, name: "ties & formalwear", parent: 106, haveChild: false),
        categoryListStruct(id: 126, name: "underwear & socks", parent: 106, haveChild: false),
        categoryListStruct(id: 127, name: "loungewear, pajamas & robes", parent: 106, haveChild: false),
        categoryListStruct(id: 128, name: "All shoes", parent: 107, haveChild: false),
        categoryListStruct(id: 129, name: "sneakers", parent: 107, haveChild: false),
        categoryListStruct(id: 130, name: "dress shoes", parent: 107, haveChild: false),
        categoryListStruct(id: 131, name: "loafers", parent: 107, haveChild: false),
        categoryListStruct(id: 132, name: "boots", parent: 107, haveChild: false),
        categoryListStruct(id: 133, name: "oxfords & lace-ups", parent: 107, haveChild: false),
        categoryListStruct(id: 134, name: "drivers", parent: 107, haveChild: false),
        categoryListStruct(id: 135, name: "flip flops & sandals", parent: 107, haveChild: false),
        categoryListStruct(id: 136, name: "All t-shirts & polos", parent: 117, haveChild: false),
        categoryListStruct(id: 137, name: "t-shirts", parent: 117, haveChild: false),
        categoryListStruct(id: 138, name: "polos", parent: 117, haveChild: false)
//        categoryListStruct(id: 139, name: "sub polos", parent: 138, haveChild: true),
//        categoryListStruct(id: 140, name: "sub sub polos", parent: 139, haveChild: true)
    ]
    
        var selectItem:(Void)->Void
    
    required init?(coder aDecoder: NSCoder) {
        self.selectItem = {Void in Void}
        super.init(coder: aDecoder)
    }

    
    @IBAction func menuButtonDidTouch(sender: AnyObject) {
        
        if toggleVar {
            toggleMenu(false)
            
            toggleVar = false
        }
        else{
            toggleMenu(true)
            
            toggleVar = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNameLabel.attributedText = NSAttributedString(string: "SHOP", attributes: NSDictionary.menu_darkGreyAttributes())
        // Do any additional setup after loading the view, typically from a nib.
        navBar.layer.shadowColor = UIColor.blackColor().colorWithAlphaComponent(0.2).CGColor
        navBar.layer.shadowOffset = CGSize(width: 0, height: 1/UIScreen.mainScreen().scale)
        navBar.layer.shadowOpacity = 1
        navBar.layer.shadowRadius = 0
        navBar.layer.masksToBounds = false
        navBar.clipsToBounds = false
    }
    
    override func viewWillAppear(animated: Bool) {
        self.changeView(UIImage(named: "img_placeholderFeature")!)
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func toggleMenu(wantsToExpand:Bool) {
        
        
        if wantsToExpand {
            menuDisplayView.hidden=false
            
//            viewNameLabel.hidden = true
//            menuButton.setImage(UIImage(named: "sym_menuX"), forState: UIControlState.Normal)
            menuButtonAnimationView.addTurnToXAnimation()
            
            if mainMenuViewController == nil {
                mainMenuViewController = storyboard?.instantiateViewControllerWithIdentifier("mainMenuVC") as? mainMenuVC
                //mainMenuViewController?.title = sectionTitles![selectedCellIndexPath!.row]
            }
            
            addChildViewController(mainMenuViewController!)
            mainMenuViewController?.categoryArray = categoryArray
            mainMenuViewController?.didMoveToParentViewController(self)
            mainMenuViewController!.view.frame = CGRectMake(0, 0, menuDisplayView.frame.width, menuDisplayView.frame.height)
//                menuDisplayView.frame.height
                //CGRect(x: 0, y: 0, width: 200, height: 200)
            menuDisplayView.addSubview(mainMenuViewController!.view)
            
            
//            mainMenuViewController?.buttonCLickedCompletion = {Void in
//                self.toggleCell(indexPath, wantsToExpand: false)
//                self.backToHomeButton.hidden = false
//                self.allButton.hidden = true
//            }
            mainMenuViewController?.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
            mainMenuViewController?.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
            mainMenuViewController?.whenIdValueChanged{
                let idValue:Int = $0
                if idValue != 0 {
                
                    self.viewNameLabel.attributedText = NSAttributedString(string:  self.categoryArray.filter{$0.id == idValue}[0].name.uppercaseString , attributes: NSDictionary.menu_darkGreyAttributes())
                }
                else{
                    self.viewNameLabel.attributedText = NSAttributedString(string:  "shop".uppercaseString , attributes: NSDictionary.menu_darkGreyAttributes())
                }
            }
            
            mainMenuViewController?.whenCloseMenuValueChanged{_ in
                
                self.toggleMenu(false)
                self.toggleVar = false
            }
            
            mainMenuViewController?.whenImageValueChanged{
                self.changeView($0)
            }
//            viewNameLabel.text =  
            searchButtonContraint.constant = 0
            mainMenuViewController?.searchConstraintTop.constant = 54
            self.mainMenuViewController!.blackOverlayView.alpha = 0
            self.mainMenuViewController!.blackOverlayViewHeightConstraint.constant = self.view.frame.height - (0.3 * self.view.frame.height)
            self.view.layoutIfNeeded()
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveLinear, animations: {
                self.mainMenuViewController!.blackOverlayView.alpha = 1
                self.searchButton.alpha = 0
                }, completion: nil)
            
            UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                self.searchButtonContraint.constant = 44
                self.mainMenuViewController?.searchConstraintTop.constant = 8
                self.viewNameLabelContraint.constant = -48
                self.viewNameLabel.alpha = 0
                
                self.mainMenuViewController!.blackOverlayViewHeightConstraint.constant = 0
                self.saksLogoBottomConstraint.constant = 44
                self.saksLogoImage.alpha = 0
                self.view.layoutIfNeeded()
                }) {
                    (isFinished) -> Void in
            }
            
            
            
        }
        else {
            
//            viewNameLabel.hidden = false
//            menuButton.setImage(UIImage(named: "sym_menuReady"), forState: UIControlState.Normal)
            menuButtonAnimationView.addTurnToHamAnimation()
            
            UIView.animateWithDuration(0.2, delay: 0.0, options: .CurveLinear, animations: {
                self.mainMenuViewController!.blackOverlayView.alpha = 0
                self.searchButton.alpha = 1
                }, completion: nil)
            
            UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                
                self.searchButtonContraint.constant = 0
                self.mainMenuViewController?.searchConstraintTop.constant = 54
                self.viewNameLabelContraint.constant = 0
                self.viewNameLabel.alpha = 1
                self.mainMenuViewController!.blackOverlayViewHeightConstraint.constant = self.view.frame.height - (0.3 * self.view.frame.height)
                self.saksLogoBottomConstraint.constant = 0
                self.saksLogoImage.alpha = 1
                self.view.layoutIfNeeded()
            
                self.menuButton.userInteractionEnabled = false
                }) {
                    (isFinished) -> Void in
                    self.menuDisplayView.hidden=true
                    self.menuButton.userInteractionEnabled = true
                    ((self.childViewControllers[0] ).view).removeFromSuperview()
                    self.childViewControllers[0].removeFromParentViewController()
            }
            
            
        }
        
        
        
    }
    @IBAction func searchButtonDidTouch(sender: AnyObject) {
        
        toggleSearch(true)
    }
    
    
    func toggleSearch(wantsToExpand:Bool) {
        
//        print ("FUCK")
        if wantsToExpand {
            menuDisplayView.hidden=false
            menuButtonAnimationView.addTurnToXAnimation()
            
            if mainSearchViewController == nil {
                mainSearchViewController = storyboard?.instantiateViewControllerWithIdentifier("mainSearchViewController") as? mainSearchVC
                //mainMenuViewController?.title = sectionTitles![selectedCellIndexPath!.row]
            }
            
            addChildViewController(mainSearchViewController!)
//            mainSearchViewController?.categoryArray = categoryArray
            mainSearchViewController?.didMoveToParentViewController(self)
            mainSearchViewController!.view.frame = CGRectMake(0, 0, menuDisplayView.frame.width, menuDisplayView.frame.height)
            //                menuDisplayView.frame.height
            //CGRect(x: 0, y: 0, width: 200, height: 200)
            menuDisplayView.addSubview(mainSearchViewController!.view)
            
            mainSearchViewController?.whenCloseMenuValueChanged{_ in
                
                self.toggleSearch(false)
//                self.toggleVar = false
            }
            
            self.mainSearchViewController?.topSearchTextContraint.constant = 8 + 44
            
            self.mainSearchViewController?.searchResultTopContraint.constant = -100
            
            self.view.layoutIfNeeded()
            self.mainSearchViewController?.searchingAreaView.alpha = 0
            self.view.layoutIfNeeded()
            
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
//                self.mainMenuViewController!.blackOverlayView.alpha = 1
                self.searchButton.alpha = 0
                self.saksLogoImage.alpha = 0
                self.saksLogoBottomConstraint.constant = 44
                self.searchButtonContraint.constant = 44
                self.mainSearchViewController?.searchingAreaView.alpha = 1
                self.mainSearchViewController?.topSearchTextContraint.constant = 8
                self.mainSearchViewController?.searchResultTopContraint.constant = 0
                
                self.view.layoutIfNeeded()
                
                }, completion: nil)
            
            
        }
        else {
            
            menuButtonAnimationView.addTurnToHamAnimation()
            self.view.endEditing(true)
            
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
                //                self.mainMenuViewController!.blackOverlayView.alpha = 1
                self.searchButton.alpha = 1
                self.saksLogoImage.alpha = 1
                self.saksLogoBottomConstraint.constant = 0
                self.searchButtonContraint.constant = 0
                self.mainSearchViewController?.searchingAreaView.alpha = 0
                self.mainSearchViewController?.topSearchTextContraint.constant = 8 + 44
                self.mainSearchViewController?.searchResultTopContraint.constant = -100
                
                self.view.layoutIfNeeded()
            }) {
                (isFinished) -> Void in
                self.menuDisplayView.hidden=true
                //            self.menuButton.userInteractionEnabled = true
                
                ((self.childViewControllers[0] ).view).removeFromSuperview()
                self.childViewControllers[0].removeFromParentViewController()
            }
            
            
            
        }
    }
    
    
    func changeView(image: UIImage){
        self.imagePlaceHolderView.image = image
        self.loadingOverlayView.alpha = 1
        
        UIView.animateWithDuration(0.2, delay: drand48() + 0.2, options: UIViewAnimationOptions.CurveLinear, animations: {
            self.loadingOverlayView.alpha = 0
            }, completion: nil)
        
    }


}

