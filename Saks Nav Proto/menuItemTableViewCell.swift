//
//  menuItemTableViewCell.swift
//  Saks Nav Proto
//
//  Created by VIRAKRI JINANGKUL on 3/24/16.
//  Copyright © 2016 VIRAKRI JINANGKUL. All rights reserved.
//

import UIKit

class menuItemTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var iconRight: UIImageView!
    @IBOutlet var overlayBlackView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.85)
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        
        if highlighted {
            self.overlayBlackView.hidden = false
//            self.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.35)
        }
        else {
            self.overlayBlackView.hidden = true
//            self.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
            
            
        }
    }

}
