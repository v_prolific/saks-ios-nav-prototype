//
//  mainMenuVC.swift
//  Saks Nav Proto
//
//  Created by VIRAKRI JINANGKUL on 3/24/16.
//  Copyright © 2016 VIRAKRI JINANGKUL. All rights reserved.
//

import UIKit

class mainMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var categoryArray = [categoryListStruct(id: 0, name: "Dummy Text", parent: 0, haveChild: false),
    categoryListStruct(id: 0, name: "Dummy Text2", parent: 0, haveChild: true),
    categoryListStruct(id: 0, name: "Dummy Text3", parent: 1, haveChild: false),
    categoryListStruct(id: 0, name: "Dummy Text4", parent: 0, haveChild: false)
    ]
    
    var mainSearchViewController: mainSearchVC?
    var subMenuVC : subMenuViewController?
    let cellHeight: CGFloat = 44
    var visibleCells: [NSIndexPath] = [NSIndexPath()]
    var sendOutIndexPath : NSIndexPath = NSIndexPath()
    
    
    @IBOutlet var arrowAnimationView: arrowRotateAnimationView!
    
    @IBOutlet var allLabelConstraint: NSLayoutConstraint!
    
    @IBAction func buttonDidTouch(sender: AnyObject) {
//        ((self.parentViewController!.childViewControllers[0] ).view).removeFromSuperview()
//        self.parentViewController?.childViewControllers[0].removeFromParentViewController()
        
//        toggleCell(visibleCells, wantsToExpand: false)
        
//        ((self.childViewControllers[0] ).view).removeFromSuperview()
//        self.childViewControllers[0].removeFromParentViewController()
    }
    @IBOutlet var allLabel: UILabel!

    @IBOutlet var searchTextField: UITextField!
    
    
    @IBOutlet var backToHomeContraint: NSLayoutConstraint!
    @IBOutlet var searchConstraintTop: NSLayoutConstraint!
    
    @IBOutlet var symArrowUpImg: UIImageView!
    @IBOutlet var textField: UITextField!
    @IBOutlet var backToHomeLabel: UILabel!
    
    @IBOutlet var backToHomeButton: UIView!
    
    @IBOutlet var allButton: UIView!
    
    @IBOutlet var tableWraperView: UIView!
    
    @IBOutlet var blackOverlayView: UIView!
    
    @IBOutlet var blackOverlayViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
        self.tableView.registerNib(UINib(nibName: "menuTableViewCell", bundle: nil), forCellReuseIdentifier: "menuTableViewCell")
        
        allLabel.attributedText = NSAttributedString(string: "ALL", attributes: NSDictionary.menu_whiteAttributes())
        allLabel.transform = CGAffineTransformMakeScale(0.8, 0.8)
        
        backToHomeLabel.attributedText = NSAttributedString(string: "HOME", attributes: NSDictionary.menu_whiteAttributes_right())
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)

        searchTextField.leftViewMode = UITextFieldViewMode.Always
        
        var magnifierImage:UIImageView?
        
        magnifierImage = UIImageView(image: UIImage(named: "sym_search_magnifier"))
        magnifierImage!.frame = CGRectMake(0, 0, 28, 12)
        
        searchTextField.borderStyle = UITextBorderStyle.None
        searchTextField.backgroundColor = UIColor(white: 1, alpha: 1)
        searchTextField.layer.cornerRadius = 4.0
        
        searchTextField.leftView = magnifierImage
        // Do any additional setup after loading the view.
        tableView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
        
    }
    
    
    @IBAction func getBackBlackButtonDidTouch(sender: AnyObject) {
        self.closeMenuCallback!(true)
    }
    
    @IBAction func allButtonDidTouch(sender: AnyObject) {
        self.toggleCell(sendOutIndexPath, wantsToExpand: false)
        
        self.imageCallback!(UIImage(named: "img_placeholderFeature")!)
        backToHomeButton.hidden = false
        self.backToHomeLabel.alpha = 0
        arrowAnimationView.addTurnToLeftAnimation()
        UIView.animateWithDuration(0.6, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.backToHomeContraint.constant = 0
            self.allLabelConstraint.constant = 44
            self.backToHomeLabel.alpha = 1
            self.allLabel.alpha = 0
            self.symArrowUpImg.alpha = 0
            self.view.layoutIfNeeded()
            }, completion: {
                (value: Bool) in
//                self.backToHomeButton.hidden = false
                self.allButton.hidden = true
        })

    }
 
    
    
    @IBAction func backToHomeDidTouch(sender: AnyObject) {
        self.closeMenuCallback!(true)
        self.idCallback!(0)
        self.imageCallback!(UIImage(named: "img_placeholderFeature")!)
    }
    
    private var idCallback: (Int -> Void)?
    func whenIdValueChanged(idCallback: (Int -> Void)) {
        self.idCallback = idCallback
    }
    
    private var closeMenuCallback: (Bool -> Void)?
    func whenCloseMenuValueChanged(closeMenuCallback: (Bool -> Void)) {
        self.closeMenuCallback = closeMenuCallback
    }
    
    private var imageCallback: (UIImage -> Void)?
    func whenImageValueChanged(imageCallback: (UIImage -> Void)) {
        self.imageCallback = imageCallback
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("menuTableViewCell") as! menuItemTableViewCell
        let row = self.categoryArray.filter{$0.parent == 0}
//        cell.title?.text = row[indexPath.row].name
        cell.title?.attributedText = NSAttributedString(string: row[indexPath.row].name.uppercaseString, attributes: NSDictionary.menu_darkGreyAttributes())
//        cell.backgroundColor = UIColor.redColor()
        
        if row[indexPath.row].haveChild {
            cell.iconRight.hidden = false
        }
        else {
            cell.iconRight.hidden = true
        }
//        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryArray.filter{$0.parent == 0}.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        /* If didselect a row and there's already an expanded cell collapse it */
//        if let unwrap_selectedCellIndexPath = selectedCellIndexPath {
//            toggleCell(unwrap_selectedCellIndexPath, wantsToExpand: false)
//        }
//            /* Expand the cell */
//        else {
//            selectedCellIndexPath = indexPath
//            toggleCell(indexPath, wantsToExpand: true)
//        }
        
        let row = self.categoryArray.filter{$0.parent == 0}
        imageCallback!(randomCDPImage(row[indexPath.row].id))
        idCallback!(row[indexPath.row].id)
        
        if row[indexPath.row].haveChild {
            toggleCell(indexPath, wantsToExpand: true)
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            backToHomeButton.hidden = false
            self.allButton.hidden = false
            self.arrowAnimationView.addTurnToUpAnimation()
            UIView.animateWithDuration(0.6, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
//                self.backToHomeContraint.constant = -44
                self.allLabelConstraint.constant = 0
                self.backToHomeLabel.alpha = 0
                self.allLabel.alpha = 1
                self.symArrowUpImg.alpha = 1
                self.view.layoutIfNeeded()
                }, completion: {
                    (value: Bool) in
                    self.backToHomeButton.hidden=true
                    
            })
            
            
            
            

        }
        else {
            
            closeMenuCallback!(true)
            //
            
//            ((self.parentViewController!.childViewControllers[0] ).view).cat
//            super.
            
            
        }
        
        
    }
    
    
    func toggleCell(indexPath:NSIndexPath, wantsToExpand:Bool) {
        if wantsToExpand {
            
            sendOutIndexPath = indexPath
            
            tableView.scrollEnabled = true//false
            
            if subMenuVC == nil {
                subMenuVC = storyboard?.instantiateViewControllerWithIdentifier("subMenuViewController") as? subMenuViewController
                
                
                print(self.categoryArray.filter{$0.parent == 0}[indexPath.row].name)
            }
            
            addChildViewController(subMenuVC!)
            
            
            
            subMenuVC?.titleMenu = self.categoryArray.filter{$0.parent == 0}[indexPath.row].name
            subMenuVC?.categoryArray = categoryArray
            subMenuVC?.parentRow = self.categoryArray.filter{$0.parent == 0}[indexPath.row].id
            subMenuVC?.view.clipsToBounds = true
            subMenuVC?.didMoveToParentViewController(self)
            
            
            
            subMenuVC?.blurView.alpha = 0
//            subMenuVC?.titleText.sizeToFit()
            subMenuVC?.titleText.alpha = 0
            
            let rectSelectedRow = tableView.rectForRowAtIndexPath(indexPath)
//            print(subMenuVC!.
            
            subMenuVC?.whenIdValueChanged{
                self.idCallback!($0)
            }
            
            subMenuVC?.whenCloseMenuValueChanged{_ in
                self.closeMenuCallback!(true)
            }
            
            subMenuVC?.whenImageValueChanged{
                self.imageCallback!($0)
            }
            
            let cellSelected = self.tableView.cellForRowAtIndexPath(indexPath)
            subMenuVC!.view.frame = CGRectMake(CGRectGetMinX(rectSelectedRow), (tableView?.convertPoint((cellSelected?.frame.origin)!, toView: tableWraperView).y)! - 44, CGRectGetWidth(rectSelectedRow), CGRectGetHeight(rectSelectedRow)+44)
            tableWraperView.addSubview(subMenuVC!.view)
            subMenuVC?.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
            subMenuVC?.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
            
            
            subMenuVC?.buttonCLickedCompletion = {Void in
                self.imageCallback!(UIImage(named: "img_placeholderFeature")!)
                self.toggleCell(indexPath, wantsToExpand: false)
                self.backToHomeButton.hidden = false
                self.arrowAnimationView.addTurnToLeftAnimation()
//                self.backToHomeButton.hidden = fa
                UIView.animateWithDuration(0.6, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                    self.tableView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
                    self.backToHomeContraint.constant = 0
                    self.allLabelConstraint.constant = 44
                    self.backToHomeLabel.alpha = 1
                    self.allLabel.alpha = 0
                    self.symArrowUpImg.alpha = 0
                    self.view.layoutIfNeeded()
                    }, completion: {
                    (value: Bool) in
                    self.allButton.hidden = true
                })
                
                
                
                
            }
           
            print(CGRectGetMinY(rectSelectedRow) - 44)
            print(tableView?.convertPoint((cellSelected?.frame.origin)!, toView: tableWraperView).y) // .convertRect((cellSelected?.frame)!, fromView: self.view))
            print("444444")
            cellSelected?.alpha = 0
            
//            print(categoryArray)
            //animation
//            print(self.tableView.estimatedRowHeight)
            self.subMenuVC!.titleText.alpha = 0
            self.subMenuVC?.shadowHeightContraint.constant = 0
            UIView.animateWithDuration(0.2, animations: {
//                self.subMenuVC!.titleDummyText.alpha = 0
                self.subMenuVC!.titleText.alpha = 1
                self.tableView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.2)
            })
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveLinear, animations: {
                
                }, completion: nil)
            
            
            UIView.animateWithDuration(0.6, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in

                self.visibleCells = []
                for item in self.tableView.visibleCells {
                    let cell = item
                    let cellIndexPath = self.tableView.indexPathForCell(cell)
                    /* move down the cells below selected one */
                    if cellIndexPath!.row > indexPath.row {
                        var frame:CGRect = cell.frame
                        frame.origin.y += self.tableView.frame.size.height - self.cellHeight * CGFloat(indexPath.row) - self.tableView.estimatedRowHeight //- 44.0
                        cell.frame = frame
                    }
                        /* move up the cells above selected one (included selected one) */
                    else {
                        var frame = cell.frame
                        frame.origin.y -= self.cellHeight * CGFloat(indexPath.row)  //- 44
                        cell.frame = frame
                    }
                    
                    /* add the cells to visibleCells array*/
                    self.visibleCells.append(cellIndexPath!)
                }

//                print(self.visibleCells)
                //Start animate
                
                self.subMenuVC?.shadowHeightContraint.constant = 6
                var frameCellExpanded = cellSelected!.frame
                frameCellExpanded.size.height = self.tableView.frame.size.height //- 44.0
                cellSelected?.frame = frameCellExpanded
                self.subMenuVC!.blurView.alpha = 1.0
                frameCellExpanded.origin.y -= 44
                frameCellExpanded.size.height += 44
                self.subMenuVC!.view.frame = frameCellExpanded
                
                let leftTitlePadding = (self.subMenuVC!.view.frame.width - self.subMenuVC!.titleText.frame.width) / 2
                
//                print(self.subMenuVC!.titleText.frame.width)
                
                print("999999")
                self.subMenuVC!.titleLeftConstraint.constant = leftTitlePadding
                self.view.layoutIfNeeded()
//                self.subMenuVC!.title
                
                return
                }) {
                    (isFinished) -> Void in
                    
                    self.subMenuVC!.titleDummyText.alpha = 0
                    self.subMenuVC!.dummyIconImage.alpha = 0
                    self.subMenuVC!.IconLeftImage.alpha = 1
                    return
            }

            
            
            
        }
        else{
            tableView.scrollEnabled = true
            self.subMenuVC!.titleDummyText.alpha = 1
            self.subMenuVC!.IconLeftImage.alpha = 0
            
            let cellSelected = self.tableView.cellForRowAtIndexPath(indexPath)
            
            UIView.animateWithDuration(0.2, animations: {
                self.subMenuVC!.overlayBlackScreen.alpha = 0
                self.subMenuVC!.dummyIconImage.alpha = 1
                
            })
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveLinear, animations: {
                //                self.subMenuVC!.titleDummyText.alpha = 1
                self.subMenuVC!.titleText.alpha = 0
                }, completion: nil)
            
            UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                for item in self.visibleCells {
                    print ("55")
                    let cell = self.tableView.cellForRowAtIndexPath(item as NSIndexPath) as! menuItemTableViewCell
                    let cellIndexPath = self.tableView.indexPathForCell(cell)
                    /* move up the cells below selected one */
                    
                    if cellIndexPath!.row > indexPath.row {
                        var frame:CGRect = cell.frame
                        frame.origin.y -= self.tableView.frame.size.height - self.cellHeight * CGFloat(indexPath.row)
                        cell.frame = frame
                    }
                        /* move down the cells above selected one (included selected one) */
                    else {
                        var frame = cell.frame
                        frame.origin.y += self.cellHeight * CGFloat(indexPath.row)
                        cell.frame = frame
                    }
                }
                
                /* Collapse selected cell */
                
                self.subMenuVC?.shadowHeightContraint.constant = 0
                
                var frameCellExpanded = cellSelected!.frame
                frameCellExpanded.size.height = 88
                cellSelected?.frame = frameCellExpanded
//                frameCellExpanded.origin.y += self.tableWraperView.frame.origin.y - 44
                frameCellExpanded.origin.y = (self.tableView?.convertPoint((cellSelected?.frame.origin)!, toView: self.tableWraperView).y)! - 44
                self.subMenuVC!.view.frame = frameCellExpanded
                
                self.subMenuVC!.blurView.alpha = 0
                
                self.subMenuVC!.titleLeftConstraint.constant = 20
                self.view.layoutIfNeeded()
                
                return
                }) {
                    (isFinished) -> Void in
                    self.tableView.reloadData()
                    ((self.childViewControllers[0] ).view).removeFromSuperview()
                    self.childViewControllers[0].removeFromParentViewController()
                    self.subMenuVC = nil
                    cellSelected?.alpha = 1
                    return
            }
        }
    }
    
    func randomCDPImage(id: Int) -> UIImage{
        var imageName: String
        var switchNumber = 0
        if (id >= 106 && id <= 138) || id == 7 {
            switchNumber = Int(drand48() * 4) + 0
        }
        else if (id >= 12 && id <= 63) || id == 2 {
            switchNumber = Int(drand48() * 4) + 4
        }
        else{
            switchNumber = Int(drand48() * 8)
        }
        
        switch switchNumber {
        case 0:
            imageName = "men001"
        case 1:
            imageName = "men002"
        case 2:
            imageName = "men003"
        case 3:
            imageName = "men004"
        case 4:
            imageName = "women001"
        case 5:
            imageName = "women002"
        case 6:
            imageName = "women003"
        case 7:
            imageName = "women004"
        default:
            imageName = "img_placeholderFeature"
        }
        return UIImage(named: imageName)!
    }

    
    @IBAction func searchButtonDidTouch(sender: AnyObject) {
        toggleSearch(true)
    }
    

    
    
    
    func toggleSearch(wantsToExpand:Bool) {
        
        //        print ("FUCK")
        if wantsToExpand {
//            menuDisplayView.hidden=false
//            menuButtonAnimationView.addTurnToXAnimation()
            
            if mainSearchViewController == nil {
                mainSearchViewController = storyboard?.instantiateViewControllerWithIdentifier("mainSearchViewController") as? mainSearchVC
                //mainMenuViewController?.title = sectionTitles![selectedCellIndexPath!.row]
            }
            
            addChildViewController(mainSearchViewController!)
            //            mainSearchViewController?.categoryArray = categoryArray
            mainSearchViewController?.didMoveToParentViewController(self)
            mainSearchViewController!.view.frame = CGRectMake(0, 0, view.frame.width, view.frame.height)
            //                menuDisplayView.frame.height
            //CGRect(x: 0, y: 0, width: 200, height: 200)
            view.addSubview(mainSearchViewController!.view)
            
            mainSearchViewController?.whenCloseMenuValueChanged{_ in
                
                self.toggleSearch(false)
                //                self.toggleVar = false
            }
            
//            self.mainSearchViewController?.topSearchTextContraint.constant = 8 + 44
            self.mainSearchViewController?.searchingAreaView.alpha = 0
            self.mainSearchViewController?.cancelButtonConstraint.constant = 10 - 50
            self.mainSearchViewController?.searchResultTopContraint.constant = -100
            self.view.layoutIfNeeded()
            
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
                //                self.mainMenuViewController!.blackOverlayView.alpha = 1
//                self.searchButton.alpha = 0
//                self.saksLogoImage.alpha = 0
//                self.saksLogoBottomConstraint.constant = 44
//                self.searchButtonContraint.constant = 44
                self.mainSearchViewController?.searchingAreaView.alpha = 1
//                self.mainSearchViewController?.topSearchTextContraint.constant = 8
                self.mainSearchViewController?.cancelButtonConstraint.constant = 10
                self.mainSearchViewController?.searchResultTopContraint.constant = 0
                
                self.view.layoutIfNeeded()
                
                }, completion: nil)
            
            
        }
        else {
            
//            menuButtonAnimationView.addTurnToHamAnimation()
            self.view.endEditing(true)
            
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseInOut, animations: {
                //                self.mainMenuViewController!.blackOverlayView.alpha = 1
//                self.searchButton.alpha = 1
//                self.saksLogoImage.alpha = 1
//                self.saksLogoBottomConstraint.constant = 0
//                self.searchButtonContraint.constant = 0
                self.mainSearchViewController?.searchingAreaView.alpha = 0
//                self.mainSearchViewController?.topSearchTextContraint.constant = 8 + 44
                self.mainSearchViewController?.cancelButtonConstraint.constant = 10 - 50
                self.mainSearchViewController?.searchResultTopContraint.constant = -100
                
                self.view.layoutIfNeeded()
            }) {
                (isFinished) -> Void in
//                self.menuDisplayView.hidden=true
                //            self.menuButton.userInteractionEnabled = true
                
                ((self.childViewControllers[0] ).view).removeFromSuperview()
                self.childViewControllers[0].removeFromParentViewController()
            }
            
            
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
