//
//  mainSearchVC.swift
//  Saks Nav Proto
//
//  Created by VIRAKRI JINANGKUL on 4/7/16.
//  Copyright © 2016 VIRAKRI JINANGKUL. All rights reserved.
//

import UIKit

class mainSearchVC: UIViewController {
    
    @IBOutlet var searchingAreaView: UIView!
    @IBOutlet var topSearchTextContraint: NSLayoutConstraint!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var searchTextField: UITextField!
    
    @IBOutlet var searchResultTopContraint: NSLayoutConstraint!
    @IBOutlet var cancelButtonConstraint: NSLayoutConstraint!
    @IBAction func cancelButtonDidTouch(sender: AnyObject) {
        self.closeMenuCallback!(true)
    }
    
    private var closeMenuCallback: (Bool -> Void)?
    func whenCloseMenuValueChanged(closeMenuCallback: (Bool -> Void)) {
        self.closeMenuCallback = closeMenuCallback
    }
    
    override func viewDidLoad() {
        
        searchTextField.leftViewMode = UITextFieldViewMode.Always
        
        var magnifierImage:UIImageView?
        
        magnifierImage = UIImageView(image: UIImage(named: "sym_search_magnifier"))
        magnifierImage!.frame = CGRectMake(0, 0, 28, 12)
        
        searchTextField.borderStyle = UITextBorderStyle.None
        searchTextField.backgroundColor = UIColor(white: 1, alpha: 1)
        searchTextField.layer.cornerRadius = 4.0
        
        searchTextField.leftView = magnifierImage
        

    }
    
    override func viewWillAppear(animated: Bool) {
        
        

        
        searchTextField.becomeFirstResponder()
        searchTextField.keyboardAppearance = UIKeyboardAppearance.Dark
        searchTextField.tintColor = UIColor.blackColor()
        searchTextField.text = ""
    }

}
