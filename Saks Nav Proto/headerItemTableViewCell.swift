//
//  headerItemTableViewCell.swift
//  Saks Nav Proto
//
//  Created by VIRAKRI JINANGKUL on 3/24/16.
//  Copyright © 2016 VIRAKRI JINANGKUL. All rights reserved.
//

import UIKit

class headerItemTableViewCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var iconLeft: UIImageView!
    @IBOutlet var iconRight: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
