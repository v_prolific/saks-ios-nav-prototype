//
//  subMenuViewController.swift
//  Saks Nav Proto
//
//  Created by VIRAKRI JINANGKUL on 3/24/16.
//  Copyright © 2016 VIRAKRI JINANGKUL. All rights reserved.
//

import UIKit

class subMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var categoryArray = [categoryListStruct(id: 0, name: "Dummy Text", parent: 0, haveChild: false),
        categoryListStruct(id: 0, name: "Dummy Text2", parent: 0, haveChild: true),
        categoryListStruct(id: 0, name: "Dummy Text3", parent: 1, haveChild: false),
        categoryListStruct(id: 0, name: "Dummy Text4", parent: 0, haveChild: false)
    ]
    
    @IBOutlet var shadowHeightContraint: NSLayoutConstraint!
    
    @IBOutlet var IconLeftImage: UIImageView!
    @IBOutlet var dummyIconImage: UIImageView!
    var subMenuVC : subMenuViewController?
    var titleMenu: String = ""
    var parentRow: Int = 0
    var buttonCLickedCompletion:(Void)->Void
    var selectItem:(Void)->Void
    let cellHeight: CGFloat = 44
    var visibleCells: [NSIndexPath] = [NSIndexPath()]
    var superMainViewController:ViewController?
    
    
    private var idCallback: (Int -> Void)?
    func whenIdValueChanged(idCallback: (Int -> Void)) {
        self.idCallback = idCallback
    }
    
    private var closeMenuCallback: (Bool -> Void)?
    func whenCloseMenuValueChanged(closeMenuCallback: (Bool -> Void)) {
        self.closeMenuCallback = closeMenuCallback
    }
    
    private var imageCallback: (UIImage -> Void)?
    func whenImageValueChanged(imageCallback: (UIImage -> Void)) {
        self.imageCallback = imageCallback
    }
    
    
    @IBOutlet var tableWraperView: UIView!
    
    @IBOutlet var blurView: UIVisualEffectView!
    @IBAction func oneLevelUpDidTouch(sender: UIButton) {
//        imageCallback!(randomCDPImage())
        self.buttonCLickedCompletion()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.buttonCLickedCompletion = {Void in Void}
        self.selectItem = {Void in Void}
        super.init(coder: aDecoder)
    }
    
    @IBOutlet var overlayBlackScreen: UIView!
    @IBOutlet var titleLeftConstraint: NSLayoutConstraint!
    
    @IBOutlet var titleView: UIView!
    
    @IBOutlet var titleDummyText: UILabel!
    @IBOutlet var titleText: UILabel!
    
    @IBOutlet var iconLeft: UIImageView!
    
    @IBOutlet var iconRight: UIImageView!
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleText.text = self.titleMenu
        self.titleDummyText.text = self.titleMenu
        
        self.titleText.attributedText = NSAttributedString(string: self.titleMenu.uppercaseString, attributes: NSDictionary.menu_darkGreyMediumAttributes())
        self.titleDummyText.attributedText = NSAttributedString(string: self.titleMenu.uppercaseString, attributes: NSDictionary.menu_darkGreyAttributes())
        
        self.titleText.sizeToFit()
        self.tableView.registerNib(UINib(nibName: "menuTableViewCell", bundle: nil), forCellReuseIdentifier: "menuTableViewCell")
//        self.titleLeftConstraint.constant= 3
        // Do any additional setup after loading the view.
        self.tableView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.85)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("menuTableViewCell") as! menuItemTableViewCell
        let row = self.categoryArray.filter{$0.parent == self.parentRow}
//        cell.title?.text = row[indexPath.row].name
        cell.title?.attributedText = NSAttributedString(string: row[indexPath.row].name.uppercaseString, attributes: NSDictionary.menu_darkGreyAttributes())
//        cell.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.65)
        
        if row[indexPath.row].haveChild {
            cell.iconRight.hidden = false
        }
        else {
            cell.iconRight.hidden = true
        }
        //        cell.selectionStyle = UITableViewCellSelectionStyle.None
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryArray.filter{$0.parent == self.parentRow}.count
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        /* If didselect a row and there's already an expanded cell collapse it */
        //        if let unwrap_selectedCellIndexPath = selectedCellIndexPath {
        //            toggleCell(unwrap_selectedCellIndexPath, wantsToExpand: false)
        //        }
        //            /* Expand the cell */
        //        else {
        //            selectedCellIndexPath = indexPath
        //            toggleCell(indexPath, wantsToExpand: true)
        //        }

        //self.childViewControllers[0].removeFromParentViewController()
//        print(indexPath)
        let row = self.categoryArray.filter{$0.parent == parentRow}
        if let imageCallback = imageCallback {
            imageCallback(randomCDPImage(row[indexPath.row].id))
        }
        
//        print(randomCDPImage())
        if let idCallback = idCallback {
            idCallback(row[indexPath.row].id)
        }
        
        if row[indexPath.row].haveChild {
            
            UIView.animateWithDuration(0.2, animations: {
                self.overlayBlackScreen.alpha = 0
            })
            
            
            toggleCell(indexPath, wantsToExpand: true)
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
//            backToHomeButton.hidden = true
//            allButton.hidden = false
        }
        else {
            //
            if let closeMenuCallback = closeMenuCallback{
                closeMenuCallback(true)
            }
            
//            print(superMainViewController)
//            superMainViewController!.categoryLabel.text = row[indexPath.row].name
//            superMainViewController!.toggleMenu(false)
//            superMainViewController!.toggleVar = false
//            performSegueWithIdentifier("toGrid", sender: self)
            
            
            
        }
        
      
    }

    
    func toggleCell(indexPath:NSIndexPath, wantsToExpand:Bool) {
        if wantsToExpand {
            
            tableView.scrollEnabled = false
            
            if subMenuVC == nil {
                subMenuVC = storyboard?.instantiateViewControllerWithIdentifier("subMenuViewController") as? subMenuViewController
                
                
                print(self.categoryArray.filter{$0.parent == parentRow}[indexPath.row].name)
            }
            
            addChildViewController(subMenuVC!)
            
            
            
            subMenuVC?.titleMenu = self.categoryArray.filter{$0.parent == parentRow}[indexPath.row].name
            subMenuVC?.categoryArray = categoryArray
            subMenuVC?.parentRow = self.categoryArray.filter{$0.parent == parentRow}[indexPath.row].id
            subMenuVC?.view.clipsToBounds = true
            subMenuVC?.didMoveToParentViewController(self)
            
            subMenuVC?.blurView.alpha = 0
            //            subMenuVC?.titleText.sizeToFit()
            //            subMenuVC?.titleText.alpha = 0
            
            let rectSelectedRow = tableView.rectForRowAtIndexPath(indexPath)
            let cellSelected = self.tableView.cellForRowAtIndexPath(indexPath)
            subMenuVC!.view.frame = CGRectMake(CGRectGetMinX(rectSelectedRow), (tableView?.convertPoint((cellSelected?.frame.origin)!, toView: view).y)! - 44, CGRectGetWidth(rectSelectedRow), CGRectGetHeight(rectSelectedRow)+44)
            view.addSubview(subMenuVC!.view)
            subMenuVC?.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 48, right: 0)
            subMenuVC?.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
            
            subMenuVC?.whenIdValueChanged{
                self.idCallback!($0)
            }
            
            subMenuVC?.whenCloseMenuValueChanged{_ in
                self.closeMenuCallback!(true)
            }
            
            subMenuVC?.buttonCLickedCompletion = {Void in
                
                if let imageCallback = self.imageCallback {
                   imageCallback(self.randomCDPImage((self.subMenuVC?.parentRow)!))
                }
                
                self.toggleCell(indexPath, wantsToExpand: false)
                UIView.animateWithDuration(0.2, animations: {
                    self.overlayBlackScreen.alpha = 1
                    self.tableView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.85)
                })
            }
            
            subMenuVC?.whenImageValueChanged{
                self.imageCallback!($0)
            }
//            let cellSelected = self.tableView.cellForRowAtIndexPath(indexPath)
            cellSelected?.alpha = 0
            
            print(categoryArray)
            //animation
            print(self.tableView.estimatedRowHeight)
            
            UIView.animateWithDuration(0.2, animations: {
                //                self.subMenuVC!.titleDummyText.alpha = 0
                self.subMenuVC!.titleText.alpha = 1
                self.tableView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0)
            })
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveLinear, animations: {
                //                self.subMenuVC!.titleText.alpha = 1
                }, completion: nil)
            
            subMenuVC?.shadowHeightContraint.constant = 0
            UIView.animateWithDuration(0.6, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: { () -> Void in
                
                self.visibleCells = []
                for item in self.tableView.visibleCells {
                    let cell = item
                    let cellIndexPath = self.tableView.indexPathForCell(cell)
                    /* move down the cells below selected one */
                    if cellIndexPath!.row > indexPath.row {
                        var frame:CGRect = cell.frame
                        frame.origin.y += self.tableView.frame.size.height - self.cellHeight * CGFloat(indexPath.row) - self.tableView.estimatedRowHeight //- 44.0
                        cell.frame = frame
                    }
                        /* move up the cells above selected one (included selected one) */
                    else {
                        var frame = cell.frame
                        frame.origin.y -= self.cellHeight * CGFloat(indexPath.row)  //- 44
                        cell.frame = frame
                    }
                    
                    /* add the cells to visibleCells array*/
                    self.visibleCells.append(cellIndexPath!)
                }
                
                print(self.visibleCells)
                //Start animate
                self.titleText.transform = CGAffineTransformMakeScale(0.8, 0.8)
//                self.subMenuVC?.titleText.transform = CGAffineTransformMakeScale(1.1, 1.1)
                self.subMenuVC?.shadowHeightContraint.constant = 6
                var frameCellExpanded = cellSelected!.frame
                frameCellExpanded.size.height = self.tableView.frame.size.height //- 44.0
                cellSelected?.frame = frameCellExpanded
                self.subMenuVC!.blurView.alpha = 1.0
//                frameCellExpanded.origin.y -= 44
//                frameCellExpanded.origin.y = self.tableWraperView.frame.origin.y - 44
                frameCellExpanded.origin.y += 44
                frameCellExpanded.size.height += 44
                
                self.subMenuVC!.view.frame = frameCellExpanded
                
                let leftTitlePadding = (self.subMenuVC!.view.frame.width - self.subMenuVC!.titleText.frame.width) / 2
                
                print(self.subMenuVC!.titleText.frame.width)
                
                print("999999")
                self.subMenuVC!.titleLeftConstraint.constant = leftTitlePadding
                self.view.layoutIfNeeded()
                //                self.subMenuVC!.title
                
                return
                }) {
                    (isFinished) -> Void in
                    
                    self.subMenuVC!.titleDummyText.alpha = 0
                    self.subMenuVC!.dummyIconImage.alpha = 0
                    self.subMenuVC!.IconLeftImage.alpha = 1
                    return
            }
            
            
            
            
        }
        else{
            tableView.scrollEnabled = true
            self.subMenuVC!.titleDummyText.alpha = 1
            self.subMenuVC!.IconLeftImage.alpha = 0
            
            let cellSelected = self.tableView.cellForRowAtIndexPath(indexPath)
            cellSelected?.alpha = 0
            UIView.animateWithDuration(0.2, animations: {
                self.subMenuVC!.overlayBlackScreen.alpha = 0
                self.subMenuVC!.dummyIconImage.alpha = 1
                
            })
            UIView.animateWithDuration(0.2, delay: 0.2, options: .CurveLinear, animations: {
                //                self.subMenuVC!.titleDummyText.alpha = 1
                self.subMenuVC!.titleText.alpha = 0
                }, completion: nil)
            
            UIView.animateWithDuration(0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                for item in self.visibleCells {
                    print ("55")
                    let cell = self.tableView.cellForRowAtIndexPath(item as NSIndexPath) as! menuItemTableViewCell
                    let cellIndexPath = self.tableView.indexPathForCell(cell)
                    /* move up the cells below selected one */
                    
                    if cellIndexPath!.row > indexPath.row {
                        var frame:CGRect = cell.frame
                        frame.origin.y -= self.tableView.frame.size.height - self.cellHeight * CGFloat(indexPath.row)
                        cell.frame = frame
                    }
                        /* move down the cells above selected one (included selected one) */
                    else {
                        var frame = cell.frame
                        frame.origin.y += self.cellHeight * CGFloat(indexPath.row)
                        cell.frame = frame
                    }
                }
                
                /* Collapse selected cell */
                self.titleText.transform = CGAffineTransformMakeScale(1, 1)
//                self.subMenuVC?.titleText.transform = CGAffineTransformMakeScale(1, 1)
                self.subMenuVC?.shadowHeightContraint.constant = 0
                var frameCellExpanded = cellSelected!.frame
                frameCellExpanded.size.height = 88
                cellSelected?.frame = frameCellExpanded
//                frameCellExpanded.origin.y += self.tableWraperView.frame.origin.y - 44
                frameCellExpanded.origin.y = (self.tableView?.convertPoint((cellSelected?.frame.origin)!, toView: self.view).y)! - 44
//                self.subMenuVC!.view.frame = frameCellExpanded
                self.subMenuVC!.view.frame = frameCellExpanded
                
                self.subMenuVC!.blurView.alpha = 0
                
                self.subMenuVC!.titleLeftConstraint.constant = 20
                self.view.layoutIfNeeded()
                
                return
                }) {
                    (isFinished) -> Void in
                    self.tableView.reloadData()
                    ((self.childViewControllers[0] ).view).removeFromSuperview()
                    self.childViewControllers[0].removeFromParentViewController()
                    self.subMenuVC = nil
                    cellSelected?.alpha = 1
                    return
            }
        }
    }
    func randomCDPImage(id: Int) -> UIImage{
        var imageName: String
        var switchNumber = 0
        if (id >= 106 && id <= 138) || id == 7 {
            switchNumber = Int(drand48() * 4) + 0
        }
        else if (id >= 12 && id <= 63) || id == 2 {
            switchNumber = Int(drand48() * 4) + 4
        }
        else{
            switchNumber = Int(drand48() * 8)
        }
        
        switch switchNumber {
        case 0:
            imageName = "men001"
        case 1:
            imageName = "men002"
        case 2:
            imageName = "men003"
        case 3:
            imageName = "men004"
        case 4:
            imageName = "women001"
        case 5:
            imageName = "women002"
        case 6:
            imageName = "women003"
        case 7:
            imageName = "women004"
        default:
            imageName = "img_placeholderFeature"
        }
        return UIImage(named: imageName)!
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
