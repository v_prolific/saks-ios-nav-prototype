//
//  menuIconAnimationView.swift
//
//  Code generated using QuartzCode 1.39.13 on 3/26/16.
//  www.quartzcodeapp.com
//

import UIKit

@IBDesignable
class menuIconAnimationView: UIView {
	
	var layers : Dictionary<String, AnyObject> = [:]
	var completionBlocks : Dictionary<CAAnimation, (Bool) -> Void> = [:]
	var updateLayerValueForCompletedAnimation : Bool = false
	
	
	
	//MARK: - Life Cycle
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupProperties()
		setupLayers()
	}
	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
		setupProperties()
		setupLayers()
	}
	
	
	
	func setupProperties(){
		
	}
	
	func setupLayers(){
		let hambergur = CALayer()
		hambergur.frame = CGRectMake(14, 16, 16, 12)
		self.layer.addSublayer(hambergur)
		layers["hambergur"] = hambergur
		let lineMiddle = CAShapeLayer()
		lineMiddle.frame = CGRectMake(0, 6, 16, 0)
		lineMiddle.path = lineMiddlePath().CGPath;
		hambergur.addSublayer(lineMiddle)
		layers["lineMiddle"] = lineMiddle
		let lineStrightButtom = CAShapeLayer()
		lineStrightButtom.frame = CGRectMake(0, 12, 16, 0)
		lineStrightButtom.path = lineStrightButtomPath().CGPath;
		hambergur.addSublayer(lineStrightButtom)
		layers["lineStrightButtom"] = lineStrightButtom
		let lineStrightTop = CAShapeLayer()
		lineStrightTop.frame = CGRectMake(0, 0, 16, 0)
		lineStrightTop.path = lineStrightTopPath().CGPath;
		hambergur.addSublayer(lineStrightTop)
		layers["lineStrightTop"] = lineStrightTop
		
		let xButton = CALayer()
		xButton.frame = CGRectMake(14.5, 14.5, 15, 15)
		self.layer.addSublayer(xButton)
		layers["xButton"] = xButton
		let lineSlantTopLeft = CAShapeLayer()
		lineSlantTopLeft.frame = CGRectMake(0, 0, 15, 15)
		lineSlantTopLeft.path = lineSlantTopLeftPath().CGPath;
		xButton.addSublayer(lineSlantTopLeft)
		layers["lineSlantTopLeft"] = lineSlantTopLeft
		let lineSlantBottomLeft = CAShapeLayer()
		lineSlantBottomLeft.frame = CGRectMake(0, 0, 15, 15)
		lineSlantBottomLeft.path = lineSlantBottomLeftPath().CGPath;
		xButton.addSublayer(lineSlantBottomLeft)
		layers["lineSlantBottomLeft"] = lineSlantBottomLeft
		
		let xExatgerate = CALayer()
		xExatgerate.frame = CGRectMake(192.5, -166.5, 15, 21)
		self.layer.addSublayer(xExatgerate)
		layers["xExatgerate"] = xExatgerate
		let path = CAShapeLayer()
		path.frame = CGRectMake(0, 0, 15, 18)
		path.path = pathPath().CGPath;
		xExatgerate.addSublayer(path)
		layers["path"] = path
		let path2 = CAShapeLayer()
		path2.frame = CGRectMake(0, 3, 15, 18)
		path2.path = path2Path().CGPath;
		xExatgerate.addSublayer(path2)
		layers["path2"] = path2
		
		let hamExatgerate = CALayer()
		hamExatgerate.frame = CGRectMake(14, 14, 16, 16)
		self.layer.addSublayer(hamExatgerate)
		layers["hamExatgerate"] = hamExatgerate
		let path3 = CAShapeLayer()
		path3.frame = CGRectMake(0, 0, 16, 2)
		path3.path = path3Path().CGPath;
		hamExatgerate.addSublayer(path3)
		layers["path3"] = path3
		let path4 = CAShapeLayer()
		path4.frame = CGRectMake(0, 14, 16, 2)
		path4.path = path4Path().CGPath;
		hamExatgerate.addSublayer(path4)
		layers["path4"] = path4
		
		resetLayerPropertiesForLayerIdentifiers(nil)
	}
	
	func resetLayerPropertiesForLayerIdentifiers(layerIds: [String]!){
		CATransaction.begin()
		CATransaction.setDisableActions(true)
		
		if layerIds == nil || layerIds.contains("lineMiddle"){
			let lineMiddle = layers["lineMiddle"] as! CAShapeLayer
			lineMiddle.fillColor   = nil
			lineMiddle.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("lineStrightButtom"){
			let lineStrightButtom = layers["lineStrightButtom"] as! CAShapeLayer
			lineStrightButtom.fillColor   = nil
			lineStrightButtom.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("lineStrightTop"){
			let lineStrightTop = layers["lineStrightTop"] as! CAShapeLayer
			lineStrightTop.fillColor   = nil
			lineStrightTop.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("lineSlantTopLeft"){
			let lineSlantTopLeft = layers["lineSlantTopLeft"] as! CAShapeLayer
			lineSlantTopLeft.hidden      = true
			lineSlantTopLeft.fillColor   = nil
			lineSlantTopLeft.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("lineSlantBottomLeft"){
			let lineSlantBottomLeft = layers["lineSlantBottomLeft"] as! CAShapeLayer
			lineSlantBottomLeft.hidden      = true
			lineSlantBottomLeft.fillColor   = nil
			lineSlantBottomLeft.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("xExatgerate"){
			let xExatgerate = layers["xExatgerate"] as! CALayer
			xExatgerate.hidden = true
			
		}
		if layerIds == nil || layerIds.contains("path"){
			let path = layers["path"] as! CAShapeLayer
			path.hidden      = true
			path.opacity     = 0
			path.fillColor   = nil
			path.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("path2"){
			let path2 = layers["path2"] as! CAShapeLayer
			path2.hidden      = true
			path2.opacity     = 0
			path2.fillColor   = nil
			path2.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("hamExatgerate"){
			let hamExatgerate = layers["hamExatgerate"] as! CALayer
			hamExatgerate.hidden = true
			
		}
		if layerIds == nil || layerIds.contains("path3"){
			let path3 = layers["path3"] as! CAShapeLayer
			path3.hidden      = true
			path3.fillColor   = nil
			path3.strokeColor = UIColor.blackColor().CGColor
		}
		if layerIds == nil || layerIds.contains("path4"){
			let path4 = layers["path4"] as! CAShapeLayer
			path4.hidden      = true
			path4.fillColor   = nil
			path4.strokeColor = UIColor.blackColor().CGColor
		}
		
		CATransaction.commit()
	}
	
	//MARK: - Animation Setup
	
	func addTurnToXAnimation(){
		addTurnToXAnimationCompletionBlock(nil)
	}
	
	func addTurnToXAnimationCompletionBlock(completionBlock: ((finished: Bool) -> Void)?){
		if completionBlock != nil{
			let completionAnim = CABasicAnimation(keyPath:"completionAnim")
			completionAnim.duration = 0.4
			completionAnim.delegate = self
			completionAnim.setValue("turnToX", forKey:"animId")
			completionAnim.setValue(false, forKey:"needEndAnim")
			layer.addAnimation(completionAnim, forKey:"turnToX")
			if let anim = layer.animationForKey("turnToX"){
				completionBlocks[anim] = completionBlock
			}
		}
		
		let fillMode : String = kCAFillModeForwards
		
		////LineMiddle animation
		let lineMiddleTransformAnim            = CAKeyframeAnimation(keyPath:"transform")
		lineMiddleTransformAnim.values         = [NSValue(CATransform3D: CATransform3DMakeTranslation(-8, 0, 0)), 
			 NSValue(CATransform3D: CATransform3DConcat(CATransform3DMakeScale(0, 1, 1), CATransform3DMakeTranslation(-8, 0, 0)))]
		lineMiddleTransformAnim.keyTimes       = [0, 1]
		lineMiddleTransformAnim.duration       = 0.217
		lineMiddleTransformAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineMiddleAnchorPointAnim      = CAKeyframeAnimation(keyPath:"anchorPoint")
		lineMiddleAnchorPointAnim.values   = [NSValue(CGPoint: CGPointMake(0, 0.5)), NSValue(CGPoint: CGPointMake(0, 0.5))]
		lineMiddleAnchorPointAnim.keyTimes = [0, 1]
		lineMiddleAnchorPointAnim.duration = 0.4
		
		let lineMiddleTurnToXAnim : CAAnimationGroup = QCMethod.groupAnimations([lineMiddleTransformAnim, lineMiddleAnchorPointAnim], fillMode:fillMode)
		layers["lineMiddle"]?.addAnimation(lineMiddleTurnToXAnim, forKey:"lineMiddleTurnToXAnim")
		
		////LineStrightButtom animation
		let lineStrightButtomHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineStrightButtomHiddenAnim.values   = [true, true]
		lineStrightButtomHiddenAnim.keyTimes = [0, 1]
		lineStrightButtomHiddenAnim.duration = 0.4
		
		let lineStrightButtomTurnToXAnim : CAAnimationGroup = QCMethod.groupAnimations([lineStrightButtomHiddenAnim], fillMode:fillMode)
		layers["lineStrightButtom"]?.addAnimation(lineStrightButtomTurnToXAnim, forKey:"lineStrightButtomTurnToXAnim")
		
		////LineStrightTop animation
		let lineStrightTopHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineStrightTopHiddenAnim.values   = [true, true]
		lineStrightTopHiddenAnim.keyTimes = [0, 1]
		lineStrightTopHiddenAnim.duration = 0.4
		
		let lineStrightTopTurnToXAnim : CAAnimationGroup = QCMethod.groupAnimations([lineStrightTopHiddenAnim], fillMode:fillMode)
		layers["lineStrightTop"]?.addAnimation(lineStrightTopTurnToXAnim, forKey:"lineStrightTopTurnToXAnim")
		
		////LineSlantTopLeft animation
		let lineSlantTopLeftPathAnim      = CAKeyframeAnimation(keyPath:"path")
		lineSlantTopLeftPathAnim.values   = [QCMethod.alignToBottomPath(lineStrightTopPath(), layer:layers["lineSlantTopLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(path2Path(), layer:layers["lineSlantTopLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(lineSlantTopLeftPath(), layer:layers["lineSlantTopLeft"] as! CALayer).CGPath]
		lineSlantTopLeftPathAnim.keyTimes = [0, 0.543, 1]
		lineSlantTopLeftPathAnim.duration = 0.4
		lineSlantTopLeftPathAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantTopLeftAnchorPointAnim    = CAKeyframeAnimation(keyPath:"anchorPoint")
		lineSlantTopLeftAnchorPointAnim.values = [NSValue(CGPoint: CGPointMake(0.55, 1.4)), NSValue(CGPoint: CGPointMake(0.5, 0.3)), NSValue(CGPoint: CGPointMake(0.5, 0.5))]
		lineSlantTopLeftAnchorPointAnim.keyTimes = [0, 0.543, 1]
		lineSlantTopLeftAnchorPointAnim.duration = 0.4
		lineSlantTopLeftAnchorPointAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantTopLeftHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineSlantTopLeftHiddenAnim.values   = [false, false]
		lineSlantTopLeftHiddenAnim.keyTimes = [0, 1]
		lineSlantTopLeftHiddenAnim.duration = 0.4
		
		let lineSlantTopLeftTurnToXAnim : CAAnimationGroup = QCMethod.groupAnimations([lineSlantTopLeftPathAnim, lineSlantTopLeftAnchorPointAnim, lineSlantTopLeftHiddenAnim], fillMode:fillMode)
		layers["lineSlantTopLeft"]?.addAnimation(lineSlantTopLeftTurnToXAnim, forKey:"lineSlantTopLeftTurnToXAnim")
		
		////LineSlantBottomLeft animation
		let lineSlantBottomLeftPathAnim      = CAKeyframeAnimation(keyPath:"path")
		lineSlantBottomLeftPathAnim.values   = [QCMethod.alignToBottomPath(lineStrightButtomPath(), layer:layers["lineSlantBottomLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(pathPath(), layer:layers["lineSlantBottomLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(lineSlantBottomLeftPath(), layer:layers["lineSlantBottomLeft"] as! CALayer).CGPath]
		lineSlantBottomLeftPathAnim.keyTimes = [0, 0.543, 1]
		lineSlantBottomLeftPathAnim.duration = 0.4
		lineSlantBottomLeftPathAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantBottomLeftAnchorPointAnim = CAKeyframeAnimation(keyPath:"anchorPoint")
		lineSlantBottomLeftAnchorPointAnim.values = [NSValue(CGPoint: CGPointMake(0.55, 0.6)), NSValue(CGPoint: CGPointMake(0.5, 0.5)), NSValue(CGPoint: CGPointMake(0.5, 0.5))]
		lineSlantBottomLeftAnchorPointAnim.keyTimes = [0, 0.543, 1]
		lineSlantBottomLeftAnchorPointAnim.duration = 0.4
		lineSlantBottomLeftAnchorPointAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantBottomLeftHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineSlantBottomLeftHiddenAnim.values   = [false, false]
		lineSlantBottomLeftHiddenAnim.keyTimes = [0, 1]
		lineSlantBottomLeftHiddenAnim.duration = 0.4
		
		let lineSlantBottomLeftTurnToXAnim : CAAnimationGroup = QCMethod.groupAnimations([lineSlantBottomLeftPathAnim, lineSlantBottomLeftAnchorPointAnim, lineSlantBottomLeftHiddenAnim], fillMode:fillMode)
		layers["lineSlantBottomLeft"]?.addAnimation(lineSlantBottomLeftTurnToXAnim, forKey:"lineSlantBottomLeftTurnToXAnim")
	}
	
	func addTurnToHamAnimation(){
		addTurnToHamAnimationCompletionBlock(nil)
	}
	
	func addTurnToHamAnimationCompletionBlock(completionBlock: ((finished: Bool) -> Void)?){
		if completionBlock != nil{
			let completionAnim = CABasicAnimation(keyPath:"completionAnim")
			completionAnim.duration = 0.4
			completionAnim.delegate = self
			completionAnim.setValue("turnToHam", forKey:"animId")
			completionAnim.setValue(false, forKey:"needEndAnim")
			layer.addAnimation(completionAnim, forKey:"turnToHam")
			if let anim = layer.animationForKey("turnToHam"){
				completionBlocks[anim] = completionBlock
			}
		}
		
		let fillMode : String = kCAFillModeForwards
		
		////LineMiddle animation
		let lineMiddleTransformAnim            = CAKeyframeAnimation(keyPath:"transform")
		lineMiddleTransformAnim.values         = [NSValue(CATransform3D: CATransform3DConcat(CATransform3DMakeScale(0, 1, 1), CATransform3DMakeTranslation(-8, 0, 0))), 
			 NSValue(CATransform3D: CATransform3DConcat(CATransform3DMakeScale(1.1, 1, 1), CATransform3DMakeTranslation(-8, 0, 0))), 
			 NSValue(CATransform3D: CATransform3DMakeTranslation(-8, 0, 0))]
		lineMiddleTransformAnim.keyTimes       = [0, 0.543, 1]
		lineMiddleTransformAnim.duration       = 0.4
		lineMiddleTransformAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineMiddleAnchorPointAnim      = CAKeyframeAnimation(keyPath:"anchorPoint")
		lineMiddleAnchorPointAnim.values   = [NSValue(CGPoint: CGPointMake(0, 0.5)), NSValue(CGPoint: CGPointMake(0, 0.5))]
		lineMiddleAnchorPointAnim.keyTimes = [0, 1]
		lineMiddleAnchorPointAnim.duration = 0.4
		
		let lineMiddleTurnToHamAnim : CAAnimationGroup = QCMethod.groupAnimations([lineMiddleTransformAnim, lineMiddleAnchorPointAnim], fillMode:fillMode)
		layers["lineMiddle"]?.addAnimation(lineMiddleTurnToHamAnim, forKey:"lineMiddleTurnToHamAnim")
		
		////LineStrightButtom animation
		let lineStrightButtomHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineStrightButtomHiddenAnim.values   = [true, true]
		lineStrightButtomHiddenAnim.keyTimes = [0, 1]
		lineStrightButtomHiddenAnim.duration = 0.4
		
		let lineStrightButtomTurnToHamAnim : CAAnimationGroup = QCMethod.groupAnimations([lineStrightButtomHiddenAnim], fillMode:fillMode)
		layers["lineStrightButtom"]?.addAnimation(lineStrightButtomTurnToHamAnim, forKey:"lineStrightButtomTurnToHamAnim")
		
		////LineStrightTop animation
		let lineStrightTopHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineStrightTopHiddenAnim.values   = [true, true]
		lineStrightTopHiddenAnim.keyTimes = [0, 1]
		lineStrightTopHiddenAnim.duration = 0.4
		
		let lineStrightTopTurnToHamAnim : CAAnimationGroup = QCMethod.groupAnimations([lineStrightTopHiddenAnim], fillMode:fillMode)
		layers["lineStrightTop"]?.addAnimation(lineStrightTopTurnToHamAnim, forKey:"lineStrightTopTurnToHamAnim")
		
		////LineSlantTopLeft animation
		let lineSlantTopLeftPathAnim      = CAKeyframeAnimation(keyPath:"path")
		lineSlantTopLeftPathAnim.values   = [QCMethod.alignToBottomPath(lineSlantTopLeftPath(), layer:layers["lineSlantTopLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(path3Path(), layer:layers["lineSlantTopLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(lineStrightTopPath(), layer:layers["lineSlantTopLeft"] as! CALayer).CGPath]
		lineSlantTopLeftPathAnim.keyTimes = [0, 0.543, 1]
		lineSlantTopLeftPathAnim.duration = 0.4
		lineSlantTopLeftPathAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantTopLeftAnchorPointAnim    = CAKeyframeAnimation(keyPath:"anchorPoint")
		lineSlantTopLeftAnchorPointAnim.values = [NSValue(CGPoint: CGPointMake(0.5, 0.5)), NSValue(CGPoint: CGPointMake(0.55, 1.4)), NSValue(CGPoint: CGPointMake(0.55, 1.4))]
		lineSlantTopLeftAnchorPointAnim.keyTimes = [0, 0.543, 1]
		lineSlantTopLeftAnchorPointAnim.duration = 0.4
		lineSlantTopLeftAnchorPointAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantTopLeftHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineSlantTopLeftHiddenAnim.values   = [false, false]
		lineSlantTopLeftHiddenAnim.keyTimes = [0, 1]
		lineSlantTopLeftHiddenAnim.duration = 0.4
		
		let lineSlantTopLeftTurnToHamAnim : CAAnimationGroup = QCMethod.groupAnimations([lineSlantTopLeftPathAnim, lineSlantTopLeftAnchorPointAnim, lineSlantTopLeftHiddenAnim], fillMode:fillMode)
		layers["lineSlantTopLeft"]?.addAnimation(lineSlantTopLeftTurnToHamAnim, forKey:"lineSlantTopLeftTurnToHamAnim")
		
		////LineSlantBottomLeft animation
		let lineSlantBottomLeftPathAnim      = CAKeyframeAnimation(keyPath:"path")
		lineSlantBottomLeftPathAnim.values   = [QCMethod.alignToBottomPath(lineSlantBottomLeftPath(), layer:layers["lineSlantBottomLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(path4Path(), layer:layers["lineSlantBottomLeft"] as! CALayer).CGPath, QCMethod.alignToBottomPath(lineStrightButtomPath(), layer:layers["lineSlantBottomLeft"] as! CALayer).CGPath]
		lineSlantBottomLeftPathAnim.keyTimes = [0, 0.543, 1]
		lineSlantBottomLeftPathAnim.duration = 0.4
		lineSlantBottomLeftPathAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantBottomLeftAnchorPointAnim = CAKeyframeAnimation(keyPath:"anchorPoint")
		lineSlantBottomLeftAnchorPointAnim.values = [NSValue(CGPoint: CGPointMake(0.5, 0.5)), NSValue(CGPoint: CGPointMake(0.55, 0.5)), NSValue(CGPoint: CGPointMake(0.55, 0.6))]
		lineSlantBottomLeftAnchorPointAnim.keyTimes = [0, 0.543, 1]
		lineSlantBottomLeftAnchorPointAnim.duration = 0.4
		lineSlantBottomLeftAnchorPointAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let lineSlantBottomLeftHiddenAnim      = CAKeyframeAnimation(keyPath:"hidden")
		lineSlantBottomLeftHiddenAnim.values   = [false, false]
		lineSlantBottomLeftHiddenAnim.keyTimes = [0, 1]
		lineSlantBottomLeftHiddenAnim.duration = 0.4
		
		let lineSlantBottomLeftTurnToHamAnim : CAAnimationGroup = QCMethod.groupAnimations([lineSlantBottomLeftPathAnim, lineSlantBottomLeftAnchorPointAnim, lineSlantBottomLeftHiddenAnim], fillMode:fillMode)
		layers["lineSlantBottomLeft"]?.addAnimation(lineSlantBottomLeftTurnToHamAnim, forKey:"lineSlantBottomLeftTurnToHamAnim")
	}
	
	//MARK: - Animation Cleanup
	
	override func animationDidStop(anim: CAAnimation, finished flag: Bool){
		if let completionBlock = completionBlocks[anim]{
			completionBlocks.removeValueForKey(anim)
			if (flag && updateLayerValueForCompletedAnimation) || anim.valueForKey("needEndAnim") as! Bool{
				updateLayerValuesForAnimationId(anim.valueForKey("animId") as! String)
				removeAnimationsForAnimationId(anim.valueForKey("animId") as! String)
			}
			completionBlock(flag)
		}
	}
	
	func updateLayerValuesForAnimationId(identifier: String){
		if identifier == "turnToX"{
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineMiddle"] as! CALayer).animationForKey("lineMiddleTurnToXAnim"), theLayer:(layers["lineMiddle"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineStrightButtom"] as! CALayer).animationForKey("lineStrightButtomTurnToXAnim"), theLayer:(layers["lineStrightButtom"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineStrightTop"] as! CALayer).animationForKey("lineStrightTopTurnToXAnim"), theLayer:(layers["lineStrightTop"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineSlantTopLeft"] as! CALayer).animationForKey("lineSlantTopLeftTurnToXAnim"), theLayer:(layers["lineSlantTopLeft"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineSlantBottomLeft"] as! CALayer).animationForKey("lineSlantBottomLeftTurnToXAnim"), theLayer:(layers["lineSlantBottomLeft"] as! CALayer))
		}
		else if identifier == "turnToHam"{
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineMiddle"] as! CALayer).animationForKey("lineMiddleTurnToHamAnim"), theLayer:(layers["lineMiddle"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineStrightButtom"] as! CALayer).animationForKey("lineStrightButtomTurnToHamAnim"), theLayer:(layers["lineStrightButtom"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineStrightTop"] as! CALayer).animationForKey("lineStrightTopTurnToHamAnim"), theLayer:(layers["lineStrightTop"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineSlantTopLeft"] as! CALayer).animationForKey("lineSlantTopLeftTurnToHamAnim"), theLayer:(layers["lineSlantTopLeft"] as! CALayer))
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["lineSlantBottomLeft"] as! CALayer).animationForKey("lineSlantBottomLeftTurnToHamAnim"), theLayer:(layers["lineSlantBottomLeft"] as! CALayer))
		}
	}
	
	func removeAnimationsForAnimationId(identifier: String){
		if identifier == "turnToX"{
			(layers["lineMiddle"] as! CALayer).removeAnimationForKey("lineMiddleTurnToXAnim")
			(layers["lineStrightButtom"] as! CALayer).removeAnimationForKey("lineStrightButtomTurnToXAnim")
			(layers["lineStrightTop"] as! CALayer).removeAnimationForKey("lineStrightTopTurnToXAnim")
			(layers["lineSlantTopLeft"] as! CALayer).removeAnimationForKey("lineSlantTopLeftTurnToXAnim")
			(layers["lineSlantBottomLeft"] as! CALayer).removeAnimationForKey("lineSlantBottomLeftTurnToXAnim")
		}
		else if identifier == "turnToHam"{
			(layers["lineMiddle"] as! CALayer).removeAnimationForKey("lineMiddleTurnToHamAnim")
			(layers["lineStrightButtom"] as! CALayer).removeAnimationForKey("lineStrightButtomTurnToHamAnim")
			(layers["lineStrightTop"] as! CALayer).removeAnimationForKey("lineStrightTopTurnToHamAnim")
			(layers["lineSlantTopLeft"] as! CALayer).removeAnimationForKey("lineSlantTopLeftTurnToHamAnim")
			(layers["lineSlantBottomLeft"] as! CALayer).removeAnimationForKey("lineSlantBottomLeftTurnToHamAnim")
		}
	}
	
	func removeAllAnimations(){
		for layer in layers.values{
			(layer as! CALayer).removeAllAnimations()
		}
	}
	
	//MARK: - Bezier Path
	
	func lineMiddlePath() -> UIBezierPath{
		let lineMiddlePath = UIBezierPath()
		lineMiddlePath.moveToPoint(CGPointMake(0, 0))
		lineMiddlePath.addLineToPoint(CGPointMake(16, 0))
		
		return lineMiddlePath;
	}
	
	func lineStrightButtomPath() -> UIBezierPath{
		let lineStrightButtomPath = UIBezierPath()
		lineStrightButtomPath.moveToPoint(CGPointMake(16, 0))
		lineStrightButtomPath.addLineToPoint(CGPointMake(0, 0))
		
		return lineStrightButtomPath;
	}
	
	func lineStrightTopPath() -> UIBezierPath{
		let lineStrightTopPath = UIBezierPath()
		lineStrightTopPath.moveToPoint(CGPointMake(16, 0))
		lineStrightTopPath.addLineToPoint(CGPointMake(0, 0))
		
		return lineStrightTopPath;
	}
	
	func lineSlantTopLeftPath() -> UIBezierPath{
		let lineSlantTopLeftPath = UIBezierPath()
		lineSlantTopLeftPath.moveToPoint(CGPointMake(15, 15))
		lineSlantTopLeftPath.addLineToPoint(CGPointMake(0, 0))
		
		return lineSlantTopLeftPath;
	}
	
	func lineSlantBottomLeftPath() -> UIBezierPath{
		let lineSlantBottomLeftPath = UIBezierPath()
		lineSlantBottomLeftPath.moveToPoint(CGPointMake(15, 0))
		lineSlantBottomLeftPath.addLineToPoint(CGPointMake(0, 15))
		
		return lineSlantBottomLeftPath;
	}
	
	func pathPath() -> UIBezierPath{
		let pathPath = UIBezierPath()
		pathPath.moveToPoint(CGPointMake(15, 0))
		pathPath.addLineToPoint(CGPointMake(0, 18))
		
		return pathPath;
	}
	
	func path2Path() -> UIBezierPath{
		let path2Path = UIBezierPath()
		path2Path.moveToPoint(CGPointMake(15, 18))
		path2Path.addLineToPoint(CGPointMake(0, 0))
		
		return path2Path;
	}
	
	func path3Path() -> UIBezierPath{
		let path3Path = UIBezierPath()
		path3Path.moveToPoint(CGPointMake(16, 0))
		path3Path.addLineToPoint(CGPointMake(0, 2))
		
		return path3Path;
	}
	
	func path4Path() -> UIBezierPath{
		let path4Path = UIBezierPath()
		path4Path.moveToPoint(CGPointMake(16, 2))
		path4Path.addLineToPoint(CGPointMake(0, 0))
		
		return path4Path;
	}
	
	
}
