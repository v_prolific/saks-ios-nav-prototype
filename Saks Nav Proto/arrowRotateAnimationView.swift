//
//  arrowRotateAnimationView.swift
//
//  Code generated using QuartzCode 1.39.13 on 3/26/16.
//  www.quartzcodeapp.com
//

import UIKit

@IBDesignable
class arrowRotateAnimationView: UIView {
	
	var layers : Dictionary<String, AnyObject> = [:]
	var completionBlocks : Dictionary<CAAnimation, (Bool) -> Void> = [:]
	var updateLayerValueForCompletedAnimation : Bool = false
	
	
	
	//MARK: - Life Cycle
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupProperties()
		setupLayers()
	}
	
	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
		setupProperties()
		setupLayers()
	}
	
	
	
	func setupProperties(){
		
	}
	
	func setupLayers(){
//		self.backgroundColor = UIColor(red:0.341, green: 0.341, blue:0.341, alpha:1)
		
		let arrowPath = CAShapeLayer()
		arrowPath.frame = CGRectMake(9, 6.47, 6, 11.06)
		arrowPath.path = arrowPathPath().CGPath;
		self.layer.addSublayer(arrowPath)
		layers["arrowPath"] = arrowPath
		
		resetLayerPropertiesForLayerIdentifiers(nil)
	}
	
	func resetLayerPropertiesForLayerIdentifiers(layerIds: [String]!){
		CATransaction.begin()
		CATransaction.setDisableActions(true)
		
		if layerIds == nil || layerIds.contains("arrowPath"){
			let arrowPath = layers["arrowPath"] as! CAShapeLayer
			arrowPath.lineCap     = kCALineCapRound
			arrowPath.lineJoin    = kCALineJoinRound
			arrowPath.fillColor   = nil
			arrowPath.strokeColor = UIColor.whiteColor().CGColor
		}
		
		CATransaction.commit()
	}
	
	//MARK: - Animation Setup
	
	func addTurnToUpAnimation(){
		addTurnToUpAnimationCompletionBlock(nil)
	}
	
	func addTurnToUpAnimationCompletionBlock(completionBlock: ((finished: Bool) -> Void)?){
		if completionBlock != nil{
			let completionAnim = CABasicAnimation(keyPath:"completionAnim")
			completionAnim.duration = 0.4
			completionAnim.delegate = self
			completionAnim.setValue("turnToUp", forKey:"animId")
			completionAnim.setValue(false, forKey:"needEndAnim")
			layer.addAnimation(completionAnim, forKey:"turnToUp")
			if let anim = layer.animationForKey("turnToUp"){
				completionBlocks[anim] = completionBlock
			}
		}
		
		let fillMode : String = kCAFillModeForwards
		
		////ArrowPath animation
		let arrowPathTransformAnim            = CAKeyframeAnimation(keyPath:"transform.rotation.z")
		arrowPathTransformAnim.values         = [0, 
			 120 * CGFloat(M_PI/180), 
			 90 * CGFloat(M_PI/180)]
		arrowPathTransformAnim.keyTimes       = [0, 0.543, 1]
		arrowPathTransformAnim.duration       = 0.4
		arrowPathTransformAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let arrowPathTurnToUpAnim : CAAnimationGroup = QCMethod.groupAnimations([arrowPathTransformAnim], fillMode:fillMode)
		layers["arrowPath"]?.addAnimation(arrowPathTurnToUpAnim, forKey:"arrowPathTurnToUpAnim")
	}
	
	func addTurnToLeftAnimation(){
		addTurnToLeftAnimationCompletionBlock(nil)
	}
	
	func addTurnToLeftAnimationCompletionBlock(completionBlock: ((finished: Bool) -> Void)?){
		if completionBlock != nil{
			let completionAnim = CABasicAnimation(keyPath:"completionAnim")
			completionAnim.duration = 0.4
			completionAnim.delegate = self
			completionAnim.setValue("turnToLeft", forKey:"animId")
			completionAnim.setValue(false, forKey:"needEndAnim")
			layer.addAnimation(completionAnim, forKey:"turnToLeft")
			if let anim = layer.animationForKey("turnToLeft"){
				completionBlocks[anim] = completionBlock
			}
		}
		
		let fillMode : String = kCAFillModeForwards
		
		////ArrowPath animation
		let arrowPathTransformAnim            = CAKeyframeAnimation(keyPath:"transform.rotation.z")
		arrowPathTransformAnim.values         = [90 * CGFloat(M_PI/180), 
			 -30 * CGFloat(M_PI/180), 
			 0]
		arrowPathTransformAnim.keyTimes       = [0, 0.543, 1]
		arrowPathTransformAnim.duration       = 0.4
		arrowPathTransformAnim.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseOut)
		
		let arrowPathTurnToLeftAnim : CAAnimationGroup = QCMethod.groupAnimations([arrowPathTransformAnim], fillMode:fillMode)
		layers["arrowPath"]?.addAnimation(arrowPathTurnToLeftAnim, forKey:"arrowPathTurnToLeftAnim")
	}
	
	//MARK: - Animation Cleanup
	
	override func animationDidStop(anim: CAAnimation, finished flag: Bool){
		if let completionBlock = completionBlocks[anim]{
			completionBlocks.removeValueForKey(anim)
			if (flag && updateLayerValueForCompletedAnimation) || anim.valueForKey("needEndAnim") as! Bool{
				updateLayerValuesForAnimationId(anim.valueForKey("animId") as! String)
				removeAnimationsForAnimationId(anim.valueForKey("animId") as! String)
			}
			completionBlock(flag)
		}
	}
	
	func updateLayerValuesForAnimationId(identifier: String){
		if identifier == "turnToUp"{
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["arrowPath"] as! CALayer).animationForKey("arrowPathTurnToUpAnim"), theLayer:(layers["arrowPath"] as! CALayer))
		}
		else if identifier == "turnToLeft"{
			QCMethod.updateValueFromPresentationLayerForAnimation((layers["arrowPath"] as! CALayer).animationForKey("arrowPathTurnToLeftAnim"), theLayer:(layers["arrowPath"] as! CALayer))
		}
	}
	
	func removeAnimationsForAnimationId(identifier: String){
		if identifier == "turnToUp"{
			(layers["arrowPath"] as! CALayer).removeAnimationForKey("arrowPathTurnToUpAnim")
		}
		else if identifier == "turnToLeft"{
			(layers["arrowPath"] as! CALayer).removeAnimationForKey("arrowPathTurnToLeftAnim")
		}
	}
	
	func removeAllAnimations(){
		for layer in layers.values{
			(layer as! CALayer).removeAllAnimations()
		}
	}
	
	//MARK: - Bezier Path
	
	func arrowPathPath() -> UIBezierPath{
		let arrowPathPath = UIBezierPath()
		arrowPathPath.moveToPoint(CGPointMake(6, 0))
		arrowPathPath.addLineToPoint(CGPointMake(0, 5.53))
		arrowPathPath.addLineToPoint(CGPointMake(6, 11.059))
		
		return arrowPathPath;
	}
	
	
}
