import UIKit

extension NSDictionary {

    static func menu_darkGreyAttributes() -> [String: AnyObject]? {
        guard let font = UIFont(name: "Gotham-Book", size: 12) else {
            fatalError("Can't load font")
        }
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.Left
        paragraph.paragraphSpacing = 0
        paragraph.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        let kern = 1
        return [NSFontAttributeName: font,
                NSKernAttributeName: kern,
                NSForegroundColorAttributeName: color,
                NSParagraphStyleAttributeName: paragraph,
        ]
    }
    
    static func menu_darkGreyMediumAttributes() -> [String: AnyObject]? {
        guard let font = UIFont(name: "Gotham-Medium", size: 12) else {
            fatalError("Can't load font")
        }
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.Left
        paragraph.paragraphSpacing = 0
        paragraph.lineBreakMode = NSLineBreakMode.ByTruncatingTail
        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let kern = 1
        return [NSFontAttributeName: font,
                NSKernAttributeName: kern,
                NSForegroundColorAttributeName: color,
                NSParagraphStyleAttributeName: paragraph,
        ]
    }
    
    static func menu_greyAttributes() -> [String: AnyObject]? {
		guard let font = UIFont(name: "Gotham-Book", size: 12) else {
			fatalError("Can't load font")
		}
		let paragraph = NSMutableParagraphStyle()
		paragraph.alignment = NSTextAlignment.Left
		paragraph.paragraphSpacing = 0
		let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
		let kern = 1
		return [NSFontAttributeName: font,
			NSKernAttributeName: kern,
			NSForegroundColorAttributeName: color,
			NSParagraphStyleAttributeName: paragraph,
		]
	}

	static func menu_whiteAttributes() -> [String: AnyObject]? {
		guard let font = UIFont(name: "Gotham-Book", size: 12) else {
			fatalError("Can't load font")
		}
		let paragraph = NSMutableParagraphStyle()
		paragraph.alignment = NSTextAlignment.Left
		paragraph.paragraphSpacing = 0
		let color = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
		let kern = 1
		return [NSFontAttributeName: font,
			NSKernAttributeName: kern,
			NSForegroundColorAttributeName: color,
			NSParagraphStyleAttributeName: paragraph,
		]
	}
    
    static func menu_whiteAttributes_right() -> [String: AnyObject]? {
        guard let font = UIFont(name: "Gotham-Book", size: 12) else {
            fatalError("Can't load font")
        }
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.Right
        paragraph.paragraphSpacing = 0
        let color = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        let kern = 1
        return [NSFontAttributeName: font,
                NSKernAttributeName: kern,
                NSForegroundColorAttributeName: color,
                NSParagraphStyleAttributeName: paragraph,
        ]
    }
    
    
}